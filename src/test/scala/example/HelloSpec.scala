package example

import org.scalatest._

class HelloSpec extends FlatSpec with Matchers {
  "The Hello object" should "say hello" in {
    Hello.greeting shouldEqual "hello"
  }

  "The version" should "accept only numbers" in {
    assertThrows[IllegalArgumentException](
      Version("x.z.y")
    )
  }

  "The version" should "be comparable" in {
//    Version(1, 2, 3).isLess(Version(1, 2, 4)) shouldBe true
  }
}
