package example

object Hello extends Greeting with App {
  println(greeting)
}

trait Greeting {
  lazy val greeting: String = "hello"
}

final case class Version(major: Int, minor: Int, patch: Int)
