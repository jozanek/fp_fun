package fp

import scala.util.Try

object Guesser {
  def parseInt(s: String): Option[Int] = Try(s.toInt).toOption
  def putStrLn(s: String): IO[Unit] = IO(() => println(s))
  def readStrLn(): IO[String] = IO(() => readLine())

  case class IO[A](usafeRun: () => A) { self =>
    def map[B](f: A => B): IO[B] = IO(() => f(self.usafeRun()))
    def flatMap[B](g: A => IO[B]): IO[B] = IO(() => g(self.usafeRun()).usafeRun())
  }

  object IO {
    def apply[A](a: A): IO[A] = IO(() => a)
  }

  def gameLoop(name: String): IO[Unit] = IO {
    var exec = true

    while (exec) {
      val num = scala.util.Random.nextInt(5) + 1

      println("Dear " + name + ", please guess a numer from 1 to 5:")

      val guess = parseInt(readLine())

      if (guess == num) println("You guessed right, " + name + "!")
      else println("You guessed wrong, " + name + "! The number was: " + num)

      println("Do you want to continue, " + name + "?")

      readLine() match {
        case "y" => exec = true
        case "n" => exec = false
        case _ => exec = false
      }
    }
  }

  def main(args: Array[String]): Unit = {

    val foo = for {
      _    <- putStrLn("What is your name?")
      name <- readStrLn()
      _    <- putStrLn("Hello " + name + " welcome to the game!")
      _    <- gameLoop(name)
    } yield ()

    foo.usafeRun()
  }
}
